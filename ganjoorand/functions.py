import pathlib
import os
import requests
import re
import ganjoorand
import random
from bs4 import BeautifulSoup
import html5lib

pwd = pathlib.Path(__file__).parent.resolve() # Path of the directory which contains the ganjoorand.py and other files

def lastSearch(): # Searches in the lastsearch file for the last user input
    try:
        with open(f"{pwd}/lastsearch") as f:
            if f.read():
                f.seek(0)
                return f.readlines()[-1] # The last line in lastsearch file
            else:
                return 0
    except FileNotFoundError: # The lastsearch file does not exist!
        open(f"{pwd}/lastsearch", 'x').close()
        return 0

def genPoemList(url, response): # Returns a list of poems' links
    response = BeautifulSoup(response.text, "html5lib")
    result = response.findAll('p', attrs={"class": "poem-excerpt"}) # Poems are in this style
    if len(result): # If the link has poems
        return result
    else:
        result = response.findAll("div", attrs={"class": "part-title-block"}) # Books and book-parts are in this style
        if len(result):
            url = "https://ganjoor.net" + random.choice(result).a["href"]
            response = requests.get(url)
            return genPoemList(url, response)
        else:
            return []

def usePoemList(url, response): # Selects one of the genPoemList generated list's items randomly and creates the URL according the selected item
    result = genPoemList(url, response)
    if len(result): # If the link has poems
        with open(f"{pwd}/lastsearch", 'a') as f:
            if ganjoorand.userinput:
                f.write(f"\n{ganjoorand.userinput}")
        url = "https://ganjoor.net" + random.choice(result).a["href"]
        return url
    else:
        return False

def cleanURL(url): # Remove extra slashes from beggining and end of the url
    while url[0]=="/":
        url = url[1::]
    while url[-1]=="/":
        url = url[:-1:]
    url = re.sub(r"\s+", "/", url) # Replace space(s) between two parts of the input if exist
    return url

def testURL(url, response): # Check if the user gived a complete URL or parts of it
    try:
        response = requests.get(url)
    except requests.exceptions.MissingSchema:
        try:
            url = "https://ganjoor.net/"+url
            response = requests.get(url)
        except Exception as err:
            return False, err
    return url, response

def openlink(url): # Opens the link in the browser
    print(url)
    open_link = input("Open the link in the browser? [Y/n] ")
    if not open_link: # If user leaves the last part empty
        os.system("xdg-open "+url) # Open the browser
