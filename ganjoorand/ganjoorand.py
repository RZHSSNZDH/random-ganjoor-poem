import sys
import functions

url = " ".join(sys.argv[1::])
response = ""
userinput = url

def getInput(url): # Gets the input from user and processes it
    if not url:
        url = input("Please enter the whole URL or the two or three end parts (e.g \"hafez ghazal\" or \"moulavi shams ghazalsh\"), you can also type the poet's name, if you leave the input empty it will be set to your last search: ")
    if not url:
        fls = functions.lastSearch() # fls: Find Last Search
        if fls:
            url = fls
        else:
            print("Searching for last search failed! Maybe there is not last search!", file=sys.stderr)
            sys.exit()
    return url

def main(url, response):
    url = getInput(url)
    url = functions.cleanURL(url)
    test = functions.testURL(url, response)
    if test[0]:
        url, response = test
    else:
        print("Invalid input:", test[1], file=sys.stderr)
    mainresult = functions.usePoemList(url, response)
    if mainresult:
        functions.openlink(mainresult)
    else:
        print("Invalid input!", file=sys.stderr)

if __name__=="__main__":
    main(url, response)
