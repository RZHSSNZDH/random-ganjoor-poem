This python script shows you a random poem from ganjoor. You can use it in two ways:
1. Enter the whole URL of the page which you want to read a random poem from it. For example to read a random ghazal from hafez: `https://ganjoor.net/hafez/ghazal/`
2. Enter the two last part of the said URL. For the above example: `hafez/ghazal` or in `hafez ghazal` form. Also `moulavi shams ghazalsh`.
3. Also you can enter only the name of the poet. In this case the app selects a random book or part of book and selects a random poem from it: `moulavi`

Then the script gives you a random poem's link and you can open it and enjoy :)

این اسکریپت پاتون به شما شعری تصادفی از گنجور را نمایش میدهد. شما میتوانید به دو صورت از آن استفاده کنید:
1. کل نشانی صفحه‌ای را که میخواهید شعری از آن را بصورت تصادفی بخوانید را وارد کنید. به عنوان نمونه برای خواندن غزلی از حافظ: `https://ganjoor.net/hafez/ghazal/`
2. تنها دو یا سه بخش انتهایی نشانی را وارد کنید. برای نمونهٔ بالا: `hafez/ghazal` یا بصورت `hafez ghazal`. همچنین `moulavi shams shazalsh`.
3. همچنین میتوانید تنها نام شاعر را وارد کنید. در این صورت برنامه یک کتاب یا یک بخش از یک کتاب را انتخاب میکند  و یک شعر از آن را انتخاب میکند: `moulavi`

سپس اسکریپت پیوندی تصادفی از یک شعر به شما میدهد و شما میتوانید آن را باز کنید و لذت ببرید :)
